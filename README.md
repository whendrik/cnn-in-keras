## Dataset - Kaggle Cats & Dogs

https://www.kaggle.com/c/dogs-vs-cats/

## General CNN

http://cs231n.github.io/convolutional-networks/

https://www.learnopencv.com/image-classification-using-convolutional-neural-networks-in-keras/

## Milestones

### History of CNN

https://www.jeremyjordan.me/convnet-architectures/

https://www.topbots.com/a-brief-history-of-neural-network-architectures/

https://medium.com/@shahariarrabby/lenet-5-alexnet-vgg-16-from-deeplearning-ai-2a4fa5f26344

https://towardsdatascience.com/history-of-convolutional-blocks-in-simple-code-96a7ddceac0c

### 1x1 Convolutions

https://iamaaditya.github.io/2016/03/one-by-one-convolution/

### 1D Convolutions

https://blog.keras.io/using-pre-trained-word-embeddings-in-a-keras-model.html

### BatchNormalization

https://towardsdatascience.com/dont-use-dropout-in-convolutional-networks-81486c823c16

### Inceptions - Stacking Convolutions

https://towardsdatascience.com/a-simple-guide-to-the-versions-of-the-inception-network-7fc52b863202

### Weakness of CNN (example)

https://hackernoon.com/capsule-networks-are-shaking-up-ai-heres-how-to-use-them-c233a0971952

http://karpathy.github.io/2015/03/30/breaking-convnets/

https://medium.com/@ageitgey/machine-learning-is-fun-part-8-how-to-intentionally-trick-neural-networks-b55da32b7196

https://medium.com/ai%C2%B3-theory-practice-business/understanding-hintons-capsule-networks-part-i-intuition-b4b559d1159b

### Make CNN's remember

https://deepmind.com/blog/wavenet-generative-model-raw-audio/