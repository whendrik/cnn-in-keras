import os
import cv2

print("Generate DATA")

IMAGES_DIRECTORY = "/CNN/all/train/"
IMAGES_OUTPUT = "/CNN/resize_output/"

CATS_OUTPUT = "/CNN/data/cats/"
DOGS_OUTPUT = "/CNN/data/dogs/"

IMAGES = os.listdir( IMAGES_DIRECTORY)
for img_filename in IMAGES:
	img_path = IMAGES_DIRECTORY + img_filename
	print(img_filename)
	cv_img = cv2.imread(img_path, cv2.IMREAD_COLOR)
	cv_resize = cv2.resize(cv_img, (52,52))
	if "dog" in img_filename:
		cv2.imwrite( DOGS_OUTPUT + img_filename, cv_resize)
	if "cat" in img_filename:
		cv2.imwrite( CATS_OUTPUT + img_filename, cv_resize)
